const express = require('express')
const bodyParser = require('body-parser')

const app = express()
app.use(bodyParser.json())
app.use(express.static('app'))

app.locals.videos = [{
	id : 1,
	name : 'test',
	email : 'test@test.com'
}, {
	id : 2,
	name : 'Maria',
	email : 'maria@test.com'
}]

app.get('/videos', (req, res) => {
	res.status(200).json(app.locals.videos)
})

app.post('/videos', (req, res) => {
	let author = req.body
	let ids = app.locals.videos.map((e) => e.id)
	author.id = Math.max(...ids) + 1
	app.locals.videos.push(author)
	res.status(201).send('created')
})

app.put('/videos/:id', (req, res) => {
	let a = req.body
	let index = app.locals.videos.findIndex((e) => e.id == req.params.id)
	if (index == -1){
		res.status(400).send('cannot edit non existent author')
	}
	else{
		app.locals.videos[index] = a
		res.status(201).send('modified')
	}
})

app.delete('/videos/:id', (req, res) => {
	let index = app.locals.videos.find((e) => e.id == req.params.id)
	if (index == -1){
		res.status(400).send('cannot delete non existent author')
	}
	else{
		app.locals.videos.splice(index,1)
		res.status(201).send('deleted')
	}
})

app.listen(8080)
